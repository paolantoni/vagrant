# Vagrant Provisioner: Centos 7 and docker compose

A Vagrant provisioner for [Docker Compose](https://docs.docker.com/compose/). Installs Docker Compose and can also bring up the containers defined by a [docker-compose.yml](https://docs.docker.com/compose/yml/).

## Vagrant box download 
This command download a Centos7 box and start it using virtualbox as provider.


```bash
vagrant init centos/7; 
vagrant up --provider virtualbox
```

A `Vagrantfile` is placed in the directory where commands are invoked.

## install the plugin
```bash
vagrant plugin install vagrant-docker-compose
```

## Edit the vagrantfile
Add the following lines to your Vagrantfile

config.vm.provision :docker
config.vm.provision :docker_compose, yml: "/<path_to_your_compose>/docker-compose.yml", rebuild: true, options: "--x-networking", command_options: { rm: "", up: "-d --timeout 20"}, run: "always"